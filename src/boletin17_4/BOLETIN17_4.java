/*
4-Realiza un programa que calcule a letra do NIF. Para eso fai o seguinte :
•	Divide o nº do DNI entre 23, sen sacar decimais, e anota o resto da división.
•	Mira na táboa asociada o resto.( exemplo: nº 1  R )
A ->3	B 11	C 20	D 9   E 22	F 7	G 4	H 18	J 13	K 21	L 19
M 5	N 12	P 8	Q 16	R 1	S 15	T 0	V 17	W 2	X 10	Y 6
Z 14.

 */
package boletin17_4;

import javax.swing.JOptionPane;

public class BOLETIN17_4 {

    public static void main(String[] args) {
        String[]letras={"A","B","C","D","E","F","G","H","J","K","L","M","N","P","Q","R","S","T","V","W","X","Y","Z"};
        int[]numeros={3,11,20,9,22,7,4,18,13,21,19,5,12,8,16,1,15,0,17,2,10,6,14};
        Metodos.calculaLetra(numeros, letras);
        
        //doutro xeito
        //char[]dni={'T','R','W'}
        //Quitamos a letra pola posición no array. char[1]=T ......
        
    }
    
}
